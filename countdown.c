///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Nicholas Tom <tom7@hawaii.edu>
// @date   02_09_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct tm *local,*gm;

void delay(int msec);

int main(int argc, char* argv[]) {

	int count = 0;
	time_t t;
	t = time(NULL);
	local = localtime(&t);

	//xxxxxxxxxxxxxxxxxxxxxxxx

	int yr = 2014;
	int mon = 1;
	int days = 21;
	int hr = 4;
	int min = 26;
	int sec = 7;
	char *ampm = "PM";

	//xxxxxxxxxxxxxxxxxxxxxxxx	

	int leap;
	int input;
	int dmon;
	char *month;
	char *day;

	switch(mon)
        {
                case 1: dmon=31; break;
                case 2: dmon=59; break;
                case 3: dmon=90; break;
                case 4: dmon=120; break;
                case 5: dmon=151; break;
                case 6: dmon=181; break;
                case 7: dmon=212; break;
                case 8: dmon=243; break;
                case 9: dmon=273; break;
                case 10: dmon=304; break;
                case 11: dmon=334; break;
                case 12: dmon=365; break;
        }
        if ((yr%4)==0&&dmon>60)
        {dmon++;}

	leap = yr / 4;
        input = (yr*365) + dmon + days + leap; //days

	switch(mon)
	{
		case 1: month="Jan"; break;
		case 2: month="Feb"; break;
		case 3: month="Mar"; break;
		case 4: month="Apr"; break;
		case 5: month="May"; break;
		case 6: month="Jun"; break;
		case 7: month="Jul"; break;
		case 8: month="Aug"; break;
		case 9: month="Sep"; break;
		case 10: month="Oct"; break;
		case 11: month="Nov"; break;
		case 12: month="Dec"; break;
	}

	switch(input%7)
        {
                case 0: day="Tue"; break;
                case 1: day="Wed"; break;
                case 2: day="Thu"; break;
                case 3: day="Fri"; break;
                case 4: day="Sat"; break;
                case 5: day="Sun"; break;
		case 6: day="Mon"; break;
       }




	printf("\nReference time:  %s %s %d %d:%d:%d %s HST %d\n\n",day,month,days,hr,min,sec,ampm,yr);

	//xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	
	if (ampm=="PM")
        {hr=hr+12;}

	while (count < 7)
        {
                 delay(1000);
                 t = time(NULL);
                 local = localtime(&t);
	int year = yr;
	int month = mon;
	int day = days;
	int hour = hr;
	int minute = min;
	int second = sec;

	int YEAR = local->tm_year+1900;
	int MONTH = local->tm_mon+1;
	int DAY = local->tm_mday;
	int HOUR = local->tm_hour;
	int MINUTE = local->tm_min;
	int SECOND = local->tm_sec;

	int y;
	int M;
	int d;
	int h;
	int m;
	int s;

	if (year == YEAR)
	{y=0;}
	if (month == MONTH)
	{M=0;}
	if (day == DAY)
	{d=0;}
	if (hour == HOUR)
	{h=0;}
	if (minute == MINUTE)
	{m=0;}
	if (second == SECOND)
	{s=0;}

		y = year - YEAR;
		M = month - MONTH;
		d = day - DAY;
		h = hour - HOUR;
		m = minute - MINUTE;
		s = second - SECOND;
	
	if (year<YEAR)
	{
		y = -y;
		M = -M;
		d = -d;
		h = -h;
		m = -m;
		s = -s;
	}

	if (s<0)
	{
		s = s + 60;
		m--;
	}
	if (m<0)
	{
		m = m + 60;
		h--;
	}
	if (h<0)
	{
		h = h + 24;
		d--;
	}
	if (d<0)
	{
		d = d + 31;
		m--;
	}
	if (m<0)
	{
		m = m + 12;
		y--;
	}




	printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n",y,d,h,m,s);
                 count++;
	}


   return 0;
}


void delay(int msec)
{
    long pause;
    clock_t now,then;

    pause = msec*(CLOCKS_PER_SEC/1000);
    now = then = clock();
    while( (now-then) < pause )
        now = clock();
}
