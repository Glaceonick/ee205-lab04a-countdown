#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct tm *local,*gm;

void delay(int msec);

int main(int argc, char* argv[]) {

        int count = 0;
        time_t t;
        t = time(NULL);
        local = localtime(&t);

        char *month;
        char *day;
        char *ampm;

        switch(local->tm_mon)
        {
                case 0: month="Mar";
                case 1: month="Apr";
                case 2: month="May";
                case 3: month="Jun";
                case 4: month="Jul";
                case 5: month="Aug";
                case 6: month="Sep";
                case 7: month="Oct";
                case 8: month="Nov";
                case 9: month="Dec";
                case 10: month="Jan";
                case 11: month="Feb";
        }

        switch(local->tm_wday)
        {
                case 0: day="Wed";
                case 1: day="Thu";
                case 2: day="Fri";
                case 3: day="Sat";
                case 4: day="Sun";
                case 5: day="Mon";
                case 6: day="Tue";
        }

        if (local->tm_hour<12)
        {ampm = "AM";}

        if (local->tm_hour>12)
        {ampm = "PM";}



        printf("\nReference time:  %s %s %d %d:%d:%d %s HST %d\n\n",day,month,local->tm_mday,local->tm_hour,local->tm_min,local->tm_sec,ampm,local->tm_year+1900);

        while (count < 10)
        {
                 delay(1000);
                 t = time(NULL);
                 local = localtime(&t);
                 printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n",local->tm_year+1900,local->tm_mday,local->tm_hour,local->tm_min,local->tm_sec);
                 count++;
        }



   return 0;
}

void delay(int msec)
{
    long pause;
    clock_t now,then;

    pause = msec*(CLOCKS_PER_SEC/1000);
    now = then = clock();
    while( (now-then) < pause )
        now = clock();
}

